# Intent and Slot-filling on the ATIS dataset task.


## A good references:

http://www.aclweb.org/anthology/N18-2118


#######################################

DESCRIPTION:

Currently, the module is meant to be used as a stand-alone module which can be used with the ATIS dataset for intent classification and slot filling.

However, this code can be customized very easily to work with any dataset, vector space, ML tasks and evaluation.
The code is modular and OOPS oriented and can be scaled to add new classes, features and parameters easily.

There are 2 broad divisions at the top level:
  1. DATA: which contains -
      1. Datasets
      2. Vector spaces created from the dataset, if required
      3. Vectorized datasets (created from the text datasets)
      4. Results - an all purpose folder to store statistics, analysis, data models and other intermediate outputs
      5. Statistics

  2. source code (src)
      - The src structure can be created into a package and used further. It contains -
        1. data_loders: a set of scripts that deal with data processing
        2. preprocessing: a set of scripts that deal with pre-processing
        3. vector_ops: a set of scripts for vector operations and dataset creation for data modelling
        4. models: a set of scripts which contains the ML models
        5. tests: scripts which should contain the unit tests for code and data integrity

########################################

TO BEGIN:

A. Installing external dependencies:

Change the current working directory to atisnlu/src and run:

      python3 setup.py develop

      - To use the libraries on a CPU (if there is no GPU), install a tensorflow binary which will allow the libraries to read data in a OS specific format
      (Note that this binary may make your tensorflow version revert back to 1.12.0.
      Get the latest binary for the latest version of tensorflow!)

      MacOS:
      pip3 install --ignore-installed --upgrade https://github.com/lakshayg/tensorflow-build/releases/download/tf1.12.0-macOS-mojave-ubuntu16.04-py2-py3/tensorflow-1.12.0-cp37-cp37m-macosx_10_13_x86_64.whl

      Other OS:
      https://github.com/lakshayg/tensorflow-build

B. To run the code:
Change the current working directory to atisnlu/src/modules and run:
      python3 run_program.py <parameters>
      OR
      python3 run_program.py --help to see the parameters to be used.

C. Running the code to create vectorized datasets from a given dataset:

      python3 run_proram.py -exp int-cls slot-fill-cls  -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess build_dataset -m rnn

      Here, (all arguments to the parameters are separated by space)
      -exp : specifies the 2 experiments to be carried out "intent classification" (int-cls) and "slot filling" (slot-fill-cls)

      -d : location of the dataset for which learning models have to be created

      -dt : the type of data within this dataset

      -da : actions to be performed on this dataset
            - read
            - preprocess
            - extract_embeddings, in case a smaller vector space should be created for the dataset for quicker executions)
            - build_dataset, in case we want to build the vectorized datasets

      -m : list of models for which the datasets have to be created. Each model might have a different requirement in terms of input-output data

      Create all dataset:
      -------------------
        python3 run_proram.py -exp int-cls slot-fill-cls -d ../../data/text_datasets/atisnlu-dev -dt train dev -da build_dataset -dsf ../../data/vector_space/atisnlu_GoogleNews2013_space.pkl -m lr nn rnn -ea fscore -mp False -gpu False

C. Running the code to run the models on vectorized datasets:

      python3 run_proram.py -exp int-cls -m rnn -ma train test evaluate -drd ../../data/ -dn atisnlu-dev -sn GoogleNew2013

      Here,
      -exp : as above

      -m : models

      -ma : model actions

      -drd : the location of the data root directory

      -dn : dataset name, useful to identify the dataset files to load

      -sn : space name, useful to identify the dataset files to load

      Run ALL models in the multiprocessing mode:
      -------------------------------------------
        python3 run_proram.py -exp int-cls slot-fill-cls -drd ../../data -dn atisnlu-dev -sn GoogleNews2013 -m lr nn rnn -ma train test evaluate -ea accuracy-one-hot fscore -mp True -gpu False

########################################

CHOICE OF MODELS :
------------------

                                  ** General **

  1. The choice of activation functions depends on the loss function being used.
     The choice of loss function, in turn, depends on the output.
        - Cross entropy goes well with Binary classification
        - Mean-Squared error goes well with Numeric outputs ()
      The activation function is also intuitively decided on the range of the component values of the input vectors.
        - [-1, 1] : tanh
        - [0, 1] : relu, softmax, sigmoid

  2. The number of hidden layers and their units / neurons depends upon the total feature space of the model. This would be an intuitive process, given that the input vectors are generally reduced (dimensionally) with no feature information

                                    ** MODELS **

1. Intent Classification:
-------------------------
    The task is to predict 1 intent per sentence given as input.
    The problem is a classical classification problem, where the input is the sentence & output is the predicted intent

    Baseline:
    ---------
    Given that input is the entire sentence, there can be a simple baseline where the input is a
    sentence vector and the output is the one-hot vector for the intent.

                1. Logistic/Linear Regression:
                ------------------------------
                  The sentence vector can be created by combining and reducing all the word vectors
                    - The sentence vector is then reduced to 300 dimensions by using PCA for consistency and normalisation in dimensions
                  The output one-hot vector is a one-hot representation of the intent

                2. Multi-Layer Neural Network:
                ------------------------------
                  This would be better than 1. because the model introduces non-linearity through hidden layer activations.
                  The sentence vector and output vector creation is the same as 1.
                  - Using Dropouts would be beneficial in case of multiple hidden layers to reduce overfitting

                - SoftMax (activation) & Categorical Crossentropy (loss funct) would be an ideal choice for both the models
                - Tanh Activation for hidden layers would be appropriate since component values are between [-1,1]

                3. CNN: (not implemented)
                -------
                A CNN can also be used to create a sentence representation to predict the one-hot-vector BUT that would be an overkill.
    RNN:
    ----
    The input of the sentence can also be treated as a sequential input with 1 word per time-step.
    However, the output is not a sequential problem. The output is a single class prediction for the entire sequence, the last time step can be used to decode/predict the one-hot vector output.

    To remove the problem of vanishing gradients, LSTM, GRU or BI-LSTM can be used for intent prediction.
    Further, dropouts between the LSTM layers can prevent overfitting.

    The depth of the network and the neurons per layer again depend on an intuitive experimentation because there is no feature information that can be extracted from the vector space.


2. Slot filling:
----------------
    The task is to identify, if and which slot applies to a particular word in a sentence.
    The problem, in essence, is a sequence based multi-label classification problem i.e. the task is to predict multiple slots per sentence AND the slot prediction can be enhanced with preceeding and following context from the sentence

    Baseline:
    ---------
    Standard ATIS dataseet baselines from previous research work

    RNN:
    ----
    An RNN would be the ideal choice for data modelling of this problem.
    - Each word of a sentence can be taken as an input timestep.
    - At each time step a corresponding output slot can be predicted
        TIMESTEP OUTPUT:
        ----------------
        There can be 2 choices for the timestep output.
        A. Categorical one-hot vector for each slot
            Here, the idea would be to predict 1 one-hot-vector @ each timestep.
            This one-hot-vector corresponds to a particular slot
        B. Scaled vector of slots per sentence
            Here, the slots can be given a numeric ID. And, an output vector of these numeric IDs can be created per sentence.
            Then, this output vector can be scaled so that each component value (slot value) falls between 0 and 1.
            This this way, the RNN can be used to predict 1 slot value at each timestep.

    - The ideal RNN here would be the BI-LSTM network because each slot prediction depends not just on the preceeding word/slot but also the following word/slot

    - Dropouts for the RNN layers should be used to prevent overfitting.

    CNN :
    -----
    Would not be an ideal choice here because while there is intra-sentence context, there is not inter-sentence context available and CNNs would performa at par with RNNs in this setup.

    If there is a scenario where the previous and following sentence also determine a current sentence's slots then a CNN should be used as the ideal model.

    However, there would be no harm in setting up a convolutional layer before the RNN layers to see if it increases the prediction accuracy.

########################################

  MODEL ARCHITECTURE:

  The Keras toolkit has been used here to create all the models.

  - The models are NOT hard-coded in the sense that the number of layers and all related and relavent parameters can be declared at program initialization and the Model Controller object can create 'n' layered deep models on demand

  - The model parameters, presently, can be declared, when the program is initialized by the
                AtisnluController.__init__() module:

                          lr':{'input_dim':[0],
                              'output_dim':[0],
                              'learning_rate':[0.01, 0.0001, 0.00001],
                              'activation_funct':[("")],
                              'output_activation_funct':["softmax"],
                              'cost_funct':["binary_crossentropy"],
                              'l2reg':[0.0, 1e-05, 1e-09],
                              'l1reg':[0.0],
                              'optimizer':["adadelta", "sgd", "adam"],
                              'max_epochs':[1000],
                              'cost_delta':[1e-06],
                              },

        * EACH parameter can have MULTIPLE "tentative" values which can be given as a list.
        * The model will create a PARAMETER STACK from all these parameters by taking a cross-product of all parameter values
        * This STACK can then be executed in parallel to find the best fit amongst all the models specified through these parameters

########################################

  EVALUATION:

  1. Intent Classification :
      Computing prediction Accuracy would work well here.

  2. Slot Filling :
      Computing Fscores are necessary here because this is a multi-label prediction problem

########################################

  STATISTICS: atisnlu/data/results/statistics
    - Contains the statistics related to datasets, spaces and vectorized datasets
