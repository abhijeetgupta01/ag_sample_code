# coding:utf-8

__author__ = "Abhijeet Gupta (abhijeet.gupta@gmail.com)"
__copyright__ = "Copyright (C) 2019 Abhijeet Gupta"
__license__ = "GPL 3.0"

###############################################################################################################
#
#  IMPORT STATEMENTS
#
###############################################################################################################


# >>>>>>>>> Native Imports <<<<<<<<<<
import os

# >>>>>>>>> Package Imports <<<<<<<<<<


# >>>>>>>>> Local Imports <<<<<<<<<<


###############################################################################################################
#
#  ACTUAL CODE
#
###############################################################################################################

class AtisnluDataReader(object):
    """
    Class to run Atisnlu dataset data processing operations

    :get_data() : return all the data read from the atisnlu dataset as a tuple (sents, slots, intents)

    """

    def __init__(self):
        """
        Method to initialize basic class-instance variables
        """
        pass
        return

    def get_data(self, data=None):
        """
        Method to extract the sentences, slots and intents from the atisnlu dataset

        :data (list(str)) : the atisnlu dataset as a list of strings

        :returns tuple(list(list(str)),list(list(str)),list(str)) : lists of sentences, slots and intents, as read from the dataset
        """
        sents = []
        slots = []
        intents = []

        for each_line in data:
            parts = each_line.strip().split("\t")
            sent = parts[0].strip().split()[1:-1]
            slot = parts[1].strip().split()[1:-1]
            intent = parts[1].strip().split()[-1]

            assert len(sent) == len(slot)

            sents.append(sent)
            slots.append(slot)
            intents.append(intent.strip())

        assert len(sents) == len(slots) == len(intents)
        # print(len(data), len(sents))
        # input("atisnlu_data_reader .. go on..")
        return (sents, slots, intents)
