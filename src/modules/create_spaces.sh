# THESE SPACES CAN BE DOWNLOADED FROM ONLINE SOURCES

# Creating various DATASET based SPACES for the experiments
  # -EXP attribute is required as a parameter but it is not used while creating the spaces
  # ONLY dataset based spaces are extracted

# DATASET : ATISNLU-DEV

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/crawl-300d-2M.vec.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/crawl-300d-2M-subword.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/GoogleNews2013.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/glove.840B.300d.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train test -da read preprocess extract_embeddings -sf ../../../../vectors/crawl-300d-2M.vec.zip

# python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-dev/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/CoNLL2017.zip

######

# DATASET : ATISNLU-TEST

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train dev -da read preprocess extract_embeddings -sf ../../../../vectors/crawl-300d-2M.vec.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train test -da read preprocess extract_embeddings -sf ../../../../vectors/crawl-300d-2M-subword.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train test -da read preprocess extract_embeddings -sf ../../../../vectors/GoogleNews2013.zip

python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train test -da read preprocess extract_embeddings -sf ../../../../vectors/glove.840B.300d.zip

# python3 run_proram.py -exp int-cls -d ../../data/text_datasets/atisnlu-test/ -dt train test -da read preprocess extract_embeddings -sf ../../../../vectors/CoNLL2017.zip

######
