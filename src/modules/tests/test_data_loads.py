###############################################################################################################
# COPYRIGHT (C) WLUPER LTD. - ALL RIGHTS RESERVED 2017 - Present
#
# UNAUTHORIZED COPYING, USE, REPRODUCTION OR DISTRIBUTION OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
# ALL CONTENTS ARE PROPRIETARY AND CONFIDENTIAL.
#
# WRITTEN BY:
#   Nikolai Rozanov <nikolai@wluper.com>
#   Bingbing Zhang <bingbing@wluper.com>
#   Ed Collins <ed@wluper.com>
#
# GENERAL ENQUIRIES:
#   <contact@wluper.com>
###############################################################################################################




###############################################################################################################
#
#  IMPORT STATEMENTS
#
###############################################################################################################

# >>>>>>>>> Native Imports <<<<<<<<<<
import os
import sys


path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(path + "/../..")


# >>>>>>>>> Package Imports <<<<<<<<<<


# >>>>>>>>> Local Imports <<<<<<<<<<
from data_loaders.data_loaders import IO


###############################################################################################################
#
#  ACTUAL CODE
#
###############################################################################################################

def test_can_load_sample_data():
    io = IO()
    sents, labels, intents = io.load(path="data/text_datasets",file="sample.csv")

    assert sents , "LOADING THE DATA WENT TERRIBLY WRONG"

    assert len(sents) == len(labels) , "WEIRD ERROR Number 1"

    assert len(sents) == len(intents) , "WEIRD ERROR Number 2"


def test_can_load_atis_data():
    io = IO()
    sents, labels, intents = io.load(path="data/text_datasets",file="atis-test.csv")

    assert sents , "LOADING THE DATA WENT TERRIBLY WRONG"

    assert len(sents) == len(labels) , "WEIRD ERROR Number 1"

    assert len(sents) == len(intents) , "WEIRD ERROR Number 2"


def test_can_load_atis2_data():
    io = IO()
    sents, labels, intents = io.load(path="data/text_datasets",file="atis-2-dev.csv")

    assert sents , "LOADING THE DATA WENT TERRIBLY WRONG"

    assert len(sents) == len(labels) , "WEIRD ERROR Number 1"

    assert len(sents) == len(intents) , "WEIRD ERROR Number 2"

###############################################################################################################
#
#  MAIN
#
###############################################################################################################


if __name__=="__main__":

    datasets = \
    {
    "1":
        {
        "1" : "atis-train.csv",
        "2" : "atis-test.csv"
        },
    "2":
        {
        "1" : "atis-2-train.csv",
        "2" : "atis-2-dev.csv"
        },
    "3":
        {
        "1" : "sample.csv"
        }
    }

    currentDataset= datasets["1"]["1"]
    io = IO()
    sents, labels, intents = io.load(path="data/text_datasets",file=currentDataset)

    print("=================================\nThe number of data points in {}: {}\n".format(currentDataset,len(sents)))


#
