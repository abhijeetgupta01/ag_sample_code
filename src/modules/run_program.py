#!/usr/local/bin/python3.7
# coding:utf-8

__author__ = "Abhijeet Gupta (abhijeet.gupta@gmail.com)"
__copyright__ = "Copyright (C) 2019 Abhijeet Gupta"
__license__ = "GPL 3.0"

###############################################################################################################
#
#  IMPORT STATEMENTS
#
###############################################################################################################

# >>>>>>>>> Native Imports <<<<<<<<<<
import os
import sys
from collections import OrderedDict

# >>>>>>>>> Package Imports <<<<<<<<<<
import argparse

# >>>>>>>>> Local Imports <<<<<<<<<<
from modules.data_ops.io import IO
from modules.preprocessing.preprocessing import DatasetPreprocessor
from modules.vector_ops.vectors import VectorOps
from modules.vector_ops.datasets import DatasetBuilder
from modules.models.model import ModelController
###############################################################################################################
#
#  ACTUAL CODE
#
###############################################################################################################

class AtisnluController(object):
    """
    Class to run the Atisnlu controller

    :run_program() : The main function to invoke the atisnlu experiment

    :returns: bool - True, if the program ran successfully
                     False, if the run failed
    """
    def __init__(self):
        self.io = IO()
        self.__multiprocessing = False
        self.__gpu = False

        # parameter dict; initialized @ __initialize_data_params()
        self.__data_params = {}

        # Pre-processing prameters - can also be read from a file
        self.__data_preprocessing_params = OrderedDict({'convert_to_lower_case':True,
                                            'convert_to_utf8':False,
                                            'check_text_vs_slot_data':True,
                                            'remove_apostrophe':True,
                                            'generate_statistics':True,
                                            })

        # Vector space parameters - can also be read from a file
        self.__data_space_params = {'l2norm':True,
                                    }

        # parameter dict; initialized @ __initialize_model_params()
        # 'experiments' & 'models' keys initialized from command line parameters

        self.__model_params = { # COMPLETE LISt OF PARAMETERS for intent classification
                                "int-cls":{"parameters":{

                                                'lr':{'input_dim':[0],
                                                    'output_dim':[0],
                                                    'learning_rate':[0.01, 0.0001, 0.00001],
                                                    'activation_funct':[("")],
                                                    'output_activation_funct':["softmax"],
                                                    'cost_funct':["binary_crossentropy"],
                                                    'l2reg':[0.0, 1e-05, 1e-09],
                                                    'l1reg':[0.0],
                                                    'optimizer':["adadelta", "sgd", "adam"],
                                                    'max_epochs':[1000],
                                                    'cost_delta':[1e-06],
                                                    },

                                               'nn':{'input_dim':[0],
                                                       'output_dim':[0],
                                                       'learning_rate':[0.01, 0.0001, 0.00001],
                                                       'hl_sizes':[(128),(128,64),(256,128,64),
                                                                   (256,128,64,32)],
                                                       'activation_funct':[("tanh"),("tanh","tanh"),
                                                                           ("tanh","tanh","tanh"),
                                                                           ("tanh","tanh","tanh","tanh"),
                                                                           ],
                                                       'output_activation_funct':["softmax"],
                                                       'cost_funct':["binary_crossentropy"],
                                                       'l2reg':[0.0, 1e-05, 1e-09],
                                                       'l1reg':[0.0],
                                                       'optimizer':["adadelta", "sgd", "adam"],
                                                       'max_epochs':[1000],
                                                       'cost_delta':[1e-06],
                                                       'dropout':[0.2,0.5]
                                                     },

                                               'rnn':{'input_dim':[0],
                                                       'output_dim':[0],
                                                       'sub_type':["bilstm", "lstm", "gru"],
                                                       'learning_rate':[0.01, 0.0001, 0.00001],
                                                       'hl_sizes':[(128),
                                                                   (128,128),
                                                                   (128,64),
                                                                   (64,64),
                                                                   (64,64,64),
                                                                   (128,64,64),
                                                                   ],
                                                       'activation_funct':[("tanh"),
                                                                           ("tanh","tanh"),
                                                                           ("tanh","tanh","tanh")
                                                                           ],
                                                       'output_activation_funct':["softmax"],
                                                       'cost_funct':["binary_crossentropy"],
                                                       'l2reg':[0.0, 1e-05, 1e-09],
                                                       'l1reg':[0.0],
                                                       'optimizer':["sgd","adam", "adadelta"],
                                                       'max_epochs':[50],
                                                       'cost_delta':[1e-06],
                                                       'dropout':[0.5],
                                                       'return_sequences':[False],
                                                       'stateful':[False],
                                                       'masking':[True]
                                                     },
                                               },

                           # Parameters for invoking padding modules
                                          'padding':{'input':True,
                                                     'output':False,
                                                     },
                                        },
                            # Complete list of parameters for slot-filling classification
                               "slot-fill-cls":{"parameters":{
                                                      'rnn':{'input_dim':[0],
                                                              'output_dim':[0],
                                                              'sub_type':["bilstm", "lstm", "gru", "simplernn"],
                                                              'learning_rate':[100.0, 10.0, 0.1, 0.01, 0.0001, 0.00001],
                                                              'hl_sizes':[(128),
                                                                          (128,128),
                                                                          (128,64),
                                                                          (64,64),
                                                                          (64,64,64),
                                                                          (128,64,64),
                                                                          (128,64,64,32),
                                                                          ],
                                                              'activation_funct':[("tanh"),
                                                                                  ("tanh","tanh"),
                                                                                  ("relu","relu"),
                                                                                  ("leakyrelu","leakyrelu"),
                                                                                  ("tanh","tanh","tanh"),
                                                                                  ("tanh","tanh","tanh","tanh"),
                                                                                  ],
                                                              'output_activation_funct':["softmax", "sigmoid", "relu"],
                                                              'cost_funct':['binary_crossentropy',
                                                                            'mean_squared_error',
                                                                            "kullback_leibler_divergence",
                                                                            'categorical_crossentropy'],
                                                              'l2reg':[0.0, 1e-05, 1e-09],
                                                              'l1reg':[0.0],
                                                              'optimizer':["sgd","adam", "adadelta"],
                                                              'max_epochs':[50],
                                                              'cost_delta':[1e-06],
                                                              'dropout':[0.0, 0.2, 0.5],
                                                              'return_sequences':[True],
                                                              'stateful':[True, False],
                                                              'masking':[True]
                                                            },
                                                      },

                                                # 'rnn':{'input_dim':[0],
                                                #         'output_dim':[0],
                                                #         'sub_type':["bilstm"],
                                                #         'learning_rate':[10.0, 0.1, 0.00001],
                                                #         'hl_sizes':[(64),
                                                #                      (128,64,32),
                                                #
                                                #                     ],
                                                #         'activation_funct':[("tanh"),
                                                #                             ("tanh","tanh","tanh"),
                                                #                             ],
                                                #         'output_activation_funct':["sigmoid","relu"],
                                                #         'cost_funct':["mean_squared_error",'categorical_crossentropy', 'kullback_leibler_divergence'],
                                                #         'l2reg':[0.000001],
                                                #         'l1reg':[0.0],
                                                #         'optimizer':["sgd", "adam", "adadelta"],
                                                #         'max_epochs':[50],
                                                #         'cost_delta':[1e-06],
                                                #         'dropout':[0.5],
                                                #         'return_sequences':[True],
                                                #         'stateful':[True,False],
                                                #         'masking':[True]
                                                #       },
                                                # },

                                    # Parameters for invoking padding modules
                                                'padding':{'input':True,
                                                           'output':True,
                                                           },
                                               }
                               }

        # parameter dict for datasets
        self.__dataset_params = {'experiments':{'int-cls':{'output_vector':'one-hot-vector',
                                                           'output_vector_dim':None,
                                                           },
                                                'slot-fill-cls':{'output_vector':'categorical_min-max-scaled',
                                                                 'output_vector_dim':None,
                                                                 },
                                                },
                                 'models':{'lr':{'input_vector':'single',
                                                 'input_vector_dim':300,
                                                 'input_vector_transformation':'pca',
                                                 },
                                           'nn':{'input_vector':'single',
                                                   'input_vector_dim':300,
                                                   'input_vector_transformation':'pca',
                                                   },
                                           'rnn':{'input_vector':'seq',
                                                  'input_vector_dim':None,
                                                  },
                                           },
                                 }

        # Data modelling parameters - can also be read from a file
        self.__dataset_space_name = "atisnlu"

        # Experiments and respective models that are allowed
        self.__experiments_allowed = {"int-cls":["lr","nn","rnn"],
                                      "slot-fill-cls":["rnn"]}

        # The inner modules which validate against this tuple(list) before running data modelling
        self.__experiments_allowed = [(each_exp, each_model) for each_exp in self.__experiments_allowed for each_model in self.__experiments_allowed[each_exp]]

        # Evaluations associated with each experiment
        self.__evaluations_allowed = {"int-cls":["accuracy-one-hot"],
                                      "slot-fill-cls":["fscore","precision", "recall"]}

        # The Evaluation class will validate against this tuple before running evaluations
        self.__evaluations_allowed = [(each_exp, each_eval) for each_exp in self.__evaluations_allowed for each_eval in self.__evaluations_allowed[each_exp]]

        self.__dataset_space_path = None

        return

    def run_program(self, commandline_args=None):
        """
        Main method to initialize the atisnlu experiment:

        DATA: All data (input, intermediate) used for data modelling is stored in atisnlu/data folder
            a. data/text_datasets: stores the dataset in textual format
            b. data/vector_space: stores the vectors (based on the dataset vocabulory) extracted from the original vector space
            c. data/vector_datasets: stores the vectorized datasets created from the text datasets

        CODE: All scripts (preprocessing, modelling, evaluations) are stored in atisnlu/src folder

        RESULTS: All evaluation results are stored atisnlu/results/<dataset_name> folder

        PROCESS (CODE IN SECTIONS BELOW):
        1. The command line parameters are validated and initialized

        2. The datasets are processed according to the dataset_actions decided
            2.1 The dataset (text) and space (embeddings) are extracted and preprocessed
            2.2 The vectorized datasets are created based on 2.1
                2.2.1 The datasets are built
                2.2.2 The mappings are created between text dataset and vectorized dataset for Evaluation

        3. The data models are created and executed according the model_actions given
            For each experiment and associated model
            3.1 Load the dataset for the model
            3.2 Create a parameter stack to be run on a specific model
            3.3 Validate the parameters against the default model specifications and other conditions
            3.4 Train (on demand - from command line)
            3.5 Test  (on demand - from command line)
            3.6 Evaluate (on demand - from command line)

        Params:
        :commandline_args (list(str)) : List of command line arguments passed from the main program

        :returns NULL, for the moment
        """
        is_executed = True

        # INITIALIZE the command line parameters
        if self.__initialize_commandline_parameters(commandline_args):

            if not self.__gpu:
                try:
                    os.environ["CUDA_VISIBLE_DEVICES"]="-1"
                except Exception as e:
                    print("Warning: Could not turn OFF the parameter which disables the use of GPU. << %s >>" % str(e))

            if ('dataset_dir' and 'dataset_files' and 'dataset_actions') in self.__data_params:
                is_executed = self.__execute_dataset_actions()
                if not is_executed:
                    raise Exception("Could not execute the DATASET actions specified")

            if ('models' and 'model_actions') in self.__model_params:
                is_executed = self.__initialize_model_actions()
                if not is_executed:
                    raise Exception("Could not execute the MODELLING actions specified")

        else:
            print("Comand line parameters could not be initialized and validated. Exiting program!")
        return is_executed

    ###############################################################################################################
    #
    #  3. EXECUTING MODELLING SPECIFIC ACTIONS
    #
    ###############################################################################################################

    def __initialize_model_actions(self, *args, **kwargs):
        """
        Method to initialize and execute all possible model actions provided as command line arguments

        :__data_params (dict(str|list)) : All dataset related parameters
        :__model_params (dict(str|list)) : All modelling parameters

        :returns (bool): True, if successfully executed
                            False, if there was a problem in running the models
        """
        is_executed = True
        # CHECKING some basic initialization that are needed for data modelling
        if ('dataset_name' or 'space_name' or 'data_root_dir') not in self.__data_params:
            is_executed = False

        if is_executed:
            if len(self.__model_params['model_actions']) > 0:
                # Consider all experiments and models given
                for each_experiment in self.__model_params['experiments']:
                    for each_model in self.__model_params['models']:

                        # RUN only those models which are validated
                        if (each_experiment, each_model) not in self.__experiments_allowed:
                            continue
                        print("Initializing model instances(s) for << %s : %s >>" % (each_experiment, each_model))

                        # Initialize the model controller (for any model)
                        self.__model = ModelController(multiprocessing=self.__multiprocessing)

                        # Initialize the model controller for a particular model
                        # Also initialize a parameter stack
                        if not self.__model.initialize_model(model=each_model,
                                                             experiment=each_experiment,
                                                             model_params=self.__model_params,
                                                             dataset_name=self.__data_params['dataset_name'],
                                                             result_dir=self.__data_params['result_dir']):
                            print("Error in initializing model << %s >>" % each_model)
                            is_executed = False

                        # Load the necessary datasets for the model
                        if not self.__model.load_datasets(dataset_name=self.__data_params['dataset_name'],
                                                          space_name=self.__data_params['space_name'],
                                                          data_root_dir=self.__data_params['data_root_dir']):
                            print("Error in loading datasets for model << %s, %s, %s, %s >>" % (each_experiment, each_model, self.__data_params['dataset_name'], self.__data_params['space_name']))
                            is_executed = False

                        # Execute the model with the provided model Actions (Train, Test or Evaluate)
                        if is_executed:

                            is_executed = self.__execute_model_actions(model_obj=self.__model,
                                                                       model_actions=self.__model_params['model_actions'],
                                                                       evaluation_actions=self.__model_params['evaluation_actions'],
                                                                       result_dir=self.__data_params['result_dir'])
                            if not is_executed:
                                print("Error in running model << %s >>" % each_model)
        return is_executed

    def __execute_model_actions(self, model_obj=None, model_actions=None,
                                evaluation_actions=None, result_dir=None):
        """
        Method to execute the model actions as specified by the command line parameters

        :model_obj (cls:ModelController): the model controller instance with the initialized model class instance
        :model_actions (list(str)): The model actions @ command line

        :return (bool) : True, if model actions were executed
                            False, if there was an error
        """
        is_executed = True
        for action in model_actions:
            if is_executed and action == "train":
                print("Training the model")
                is_executed = model_obj.train(result_dir=result_dir)
            if is_executed and action == "test":
                print("Testing the model")
                is_executed = model_obj.test(result_dir=result_dir)
            if is_executed and action == "evaluate":
                print("Evaluating the model")
                is_executed = model_obj.evaluate(result_dir=result_dir,
                                                 evaluation_actions=evaluation_actions)

            if not is_executed:
                print("Error: Failed to execute << %s >> action." % action)
                break

        return is_executed

    ###############################################################################################################
    #
    #  2. EXECUTING DATASET SPECIFIC ACTIONS
    #
    ###############################################################################################################

    def __execute_dataset_actions(self, *args, **kwargs):
        """
        Method to execute all possible dataset actions provided as command line arguments

        :__data_params (dict(str|list)) : All dataset related parameters

        :returns (bool) : True, if all actions could be executed
                          False, if any of the actions could not be successfully executed
        """
        is_executed = True
        data = None

        if "read" in self.__data_params['dataset_actions']:
            print("Reading dataset")
            is_executed, data = self.__execute_dataset_actions_read_data()

        if (is_executed and data) and "preprocess" in self.__data_params['dataset_actions']:
            print("Preprocessing dataset")
            is_executed, data = self.__execute_dataset_actions_preprocess_data(data)

        if is_executed and "extract_embeddings" in self.__data_params['dataset_actions']:
            print("Extracting embeddings from the vector space")
            is_executed = self.__execute_dataset_actions_extract_embeddings(data)

        if is_executed and "build_dataset" in self.__data_params['dataset_actions']:
            print("Building the dataset")
            is_executed = self.__execute_dataset_actions_build_dataset(data)

        return is_executed

    def __execute_dataset_actions_read_data(self):
        """
        Method to read the dataset files

        :return bool, (dict(list(str))) :
            bool: True, False - depending on whether data could be read
            (dict(list(str))) : a data dictionary which contains the data of the files read
        """

        data = {}
        is_executed = True

        for dataset_type, file_name in self.__data_params['dataset_files'].items():
            data[dataset_type] = self.io.read_file(path=os.path.dirname(file_name),
                                                   filename=os.path.basename(file_name), format=os.path.splitext(file_name)[-1].replace(".","").strip())
            if data[dataset_type] is None:
                is_executed = False

        return is_executed, data

    def __execute_dataset_actions_preprocess_data(self, data):
        """
        Method to preprocess a dataset
        ONLY TEXTUAL PREPROCESSING OF THE DATASET

        :data (dict(list(str))) : the original dataset, as read from the file

        :returns bool, (dict(list(str))) :
            bool: True, False - depending on whether data could be pre-processed
            (dict(list(str))) : a data dictionary which contains the pre-processed data
        """
        if len(self.__data_preprocessing_params) > 0:
            pre_processor = DatasetPreprocessor()
            for dataset_type, file_data in data.items():
                is_executed, pre_processed_data = pre_processor.run(data=file_data,
                                                                    dataset_type=dataset_type,
                                                                    pre_processing_params=self.__data_preprocessing_params,
                                                                    data_params=self.__data_params)
                if not is_executed:
                    break
                data[dataset_type] = pre_processed_data

        return is_executed, data

    def __execute_dataset_actions_extract_embeddings(self, data):
        """
        Method to extract the (downloaded) embeddings from a space to a
        smaller sub-space based on the dataset elements.

        :data (dict(str:list(str))) : Original OR the pre-processed dataset

        :returns NULL
        """
        # Initialize basic variables
        #################################################################################################
        # Original Space related variables
        space_path = os.path.dirname(self.__data_params['space_file'])
        space_file_name = os.path.basename(self.__data_params['space_file'])
        space_name, space_extension = os.path.splitext(space_file_name)
        space_extension = space_extension.replace(".","")

        dataset_name = self.__data_params['dataset_name']

        # Dataset space related variables
        self.__dataset_space_path = self.__data_params['vector_space']
        self.__dataset_space_name += "_" + space_name

        # New space instances
        vector_ops = VectorOps()

        #################################################################################################

        # Extract embeddings from the given (downloaded) embeddings
        is_executed = vector_ops.build_space(data=data, dataset_name=dataset_name, space_path=space_path,
                                             space_file_name=space_file_name, space_name=space_name, space_extension=space_extension)
        # Applying space transformations
        if is_executed:
            is_executed = vector_ops.apply_space_transformations(transformation_params=self.__data_space_params)

        # Saving the dataset space
        if is_executed:
            is_executed = vector_ops.save_space(space_path=self.__dataset_space_path,
                                                space_name=self.__dataset_space_name)

        #################################################################################################

        # Saving dataset related space statistics
        if is_executed:
            result_dir = self.__data_params['result_statistics_dir']
            statistics_file_name = self.__dataset_space_name + "_statistics.json"

            if os.path.exists(os.path.join(result_dir, statistics_file_name)):
                prev_space_params = self.io.read_file(path=result_dir, filename=statistics_file_name,
                                                      format="json")
                if dataset_name in prev_space_params:
                    prev_space_params[dataset_name].update(vector_ops.space_params[dataset_name])
                else:
                    prev_space_params[dataset_name] = vector_ops.space_params[dataset_name]

                if not self.io.write_file(path=result_dir, filename=statistics_file_name, format="json",
                                   data=prev_space_params):
                    is_executed = False
            else:
                if not self.io.write_file(path=result_dir, filename=statistics_file_name, format="json",
                                   data=vector_ops.space_params):
                    is_executed = False

        return is_executed

    def __execute_dataset_actions_build_dataset(self, data):
        """
        Method to build a vectorized dataset from a given text dataset and its vocabulory's space built prior to this step
        - The dataset is built with respect to the experiment being carried out and then with respect to the models that have different input/output requirements
        - ASSUMPTION: Currently, there is a m x n relation between the experiments and the models given as input

        :data (dict(str:list(str))) : the original or preprocessed data passed to the function

        :returns (bool): True, if the datasets were built successfully
                            False, if the datasets were not built
        """
        is_executed = True

        # Validating the variables required to build the datasets
        if data is None:
            is_executed = False
            print("No 'data' to build datasets")

        elif self.__dataset_space_name is None:
            is_executed = False
            print("Set __dataset_space_name in AtisnluController.__init__()")

        elif self.__model_params['experiments'] is None or len(self.__model_params['experiments']) < 1:
            is_executed = False
            print("Experiments provided cannot be of NoneType or NULL strings")

        elif self.__model_params['models'] is None or len(self.__model_params['models']) < 1:
            is_executed = False
            print("Models provided cannot be of NoneType or NULL strings")

        elif len(self.io.search_files(path=self.__data_params['vector_space'],
                                      search_string=self.__dataset_space_name,
                                      match_type="partial")) > 1:
            if "dataset_space_file" not in self.__data_params:
                is_executed = False
                print(self.io.search_files(path=self.__data_params['vector_space'],
                                           search_string=self.__dataset_space_name,
                                           match_type="partial"))
                print("More than 1 Dataset Space File found for this dataset (above).\n\tSpecify the right Dataset Space File to use by initializing the -dsf parameter with a space file location")

        if is_executed:
            if "dataset_space_file" in self.__data_params:
                space_file = self.__data_params['dataset_space_file']
            else:
                files = self.io.search_files(path=self.__data_params['vector_space'],
                                              search_string=self.__dataset_space_name,
                                              match_type="partial")
                if len(files) < 1:
                    is_executed = False
                    print("No dataset space file found!")
                elif len(files) > 1:
                    is_executed = False
                    print("Set dataset_space_file from (dsf) parameter")
                else:
                    space_file = files[0]

            if is_executed:
                if not os.path.isfile(space_file):
                    is_executed = False
                    print("<< %s >> does not seem like a valid file!" % space_file)


            if is_executed:
                dataset_builder = DatasetBuilder()
                is_executed = dataset_builder.build_dataset(dataset_name=self.__data_params['dataset_name'],
                                                            data=data, space_file=space_file,
                                                            data_params=self.__data_params,
                                                            dataset_params=self.__dataset_params,
                                                            model_params=self.__model_params,
                                                            experiments_allowed=self.__experiments_allowed
                                                            )


        return is_executed

    ###############################################################################################################
    #
    #  1. INITIALIZING AND VALIDATING COMMAND LINE ARGUMENTS
    #
    ###############################################################################################################

    def __initialize_commandline_parameters(self, commandline_args):
        """
        Method to validate the command line arguments passed.
        If required, these parameters will initialize more in-program parameters

        :commandline_args (list(str)) : list of command line arguments
            to be parsed by the argparse package

        :returns (bool): True, if arguments are validated
                         False, if arguments are not validated
        """
        # Parse the arguments
        parsed_args = self.__parse_arguments(commandline_args)

        # FLAG - gives the go-ahead to run the program, if all initializations are correct
        are_validated = True

        if not parsed_args.experiment:
            are_validated = False
            print("Program cannot run without an experiment being specified.\nType python3 run_program.py --help")
        else:
            if parsed_args.experiment is None or len(parsed_args.experiment) < 1:
                are_validated = False
                print("Experiments provided cannot be of NoneType of NULL strings")
                self.__data_params['experiments'] = None
                self.__model_params['experiments'] = None
            else:
                self.__data_params['experiments'] = parsed_args.experiment
                self.__model_params['experiments'] = parsed_args.experiment

            # INITIALIZE data and model parameters
            self.__initialize_data_params(are_validated, parsed_args)
            self.__initialize_model_params(are_validated, parsed_args)

            if len(self.__data_params) == len(self.__model_params) == 1:
                are_validated = False
                print("No data OR model parameters given/initialized!")

            # INITIALIZE misc params - for the moment
            if parsed_args.verbose:
                self.__verbosity_limit = parsed_args.verbose

            # In case A DATAASET is not initialized and we want to perform ONLY modelling based actions
            # Then the following data_params need to be initialized
            if not parsed_args.dataset_dir:
                if parsed_args.data_root_dir:
                    self.__data_params['data_root_dir'] = os.path.abspath(parsed_args.data_root_dir[0])
                    if not os.path.isdir(self.__data_params['data_root_dir']):
                        os.makedirs(self.__data_params['data_root_dir'])
                    result_dir = os.path.join(self.__data_params['data_root_dir'], "results")
                    result_statistics_dir = os.path.join(self.__data_params['data_root_dir'], "results", "statistics")
                    vector_space = os.path.join(self.__data_params['data_root_dir'], "vector_space")
                    vector_datasets = os.path.join(self.__data_params['data_root_dir'], "vector_datasets")
                    if not os.path.isdir(result_dir):
                        os.makedirs(result_dir)
                    self.__data_params['result_dir'] = result_dir
                    if not os.path.isdir(result_statistics_dir):
                        os.makedirs(result_statistics_dir)
                    self.__data_params['result_statistics_dir'] = result_statistics_dir
                    if not os.path.isdir(vector_space):
                        os.makedirs(vector_space)
                    self.__data_params['vector_space'] = vector_space
                    if not os.path.isdir(vector_datasets):
                        os.makedirs(vector_datasets)
                    self.__data_params['vector_datasets'] = vector_datasets
                else:
                    are_validated = False

            if parsed_args.dataset_name:
                self.__data_params['dataset_name'] = parsed_args.dataset_name[0]

            if parsed_args.space_name:
                self.__data_params['space_name'] = parsed_args.space_name[0]

            if not parsed_args.multi_processing or parsed_args.multi_processing is None or (parsed_args.multi_processing and parsed_args.multi_processing[0] not in ["True", "False"]):
                are_validated = False
                print("parsed_args.multi_processing requires a True or a False value to enable or disable the  multi-processing mode")
                self.__multiprocessing = False
            else:
                if parsed_args.multi_processing[0] == "True":
                    self.__multiprocessing = True
                else:
                    self.__multiprocessing = False

            if not parsed_args.gpu or parsed_args.gpu is None or (parsed_args.gpu and parsed_args.gpu[0] not in ["True", "False"]):
                are_validated = False
                print("parsed_args.gpu requires a True or a False value to enable or disable the script from running on the gpu")
                self.__gpu = False
            else:
                if parsed_args.gpu[0] == "True":
                    self.__gpu = True
                else:
                    self.__gpu = False

        """
        IF all command line parameters have been validated, then carry out some additional validations
        on inter-related parameters
        """
        if are_validated:
            are_validated = self.__final_validation(are_validated, parsed_args)
        return are_validated

    def __parse_arguments(self, commandline_args):
        """
        Method to parse the arguments according to argparse library

        :commandline_args (list(str)) : list of command line arguments

        :returns Namespace(option=list(str)|list(int)|str)
        """
        parser = argparse.ArgumentParser(description="This program runs uses the ATIS dataset to train and test speific ML models.\n\tUsage: python3 run_program.py [options]")

        # GLOBAL MANDATORY PARAMS

        parser.add_argument("-exp", "--experiment", help="The experiments to be executed. Choices: int-cls slot-fill-cls", nargs="*", type=str, choices=["int-cls", "slot-fill-cls"])

        # DATASET PARAMS

        parser.add_argument("-d", "--dataset_dir", help="Location of the dataset folder", nargs=1, type=str)

        parser.add_argument("-dt", "--dataset_type", help="The instances of dataset that should be read for data. Choices: train dev test", nargs="*", type=str, choices=["train", "test", "dev"])

        parser.add_argument("-da", "--dataset_action", help="The actions to be performed on each of the dataset type. Choices: read preprocess extract_embeddings build_dataset", nargs="*", type=str, choices=["read", "preprocess", "extract_embeddings", "build_dataset"])

        parser.add_argument("-sf", "--space_file", help="The file which contains the original embedding space", nargs=1, type=str)

        parser.add_argument("-dsf", "--dataset_space_file", help="The file which contains the dataset space extracted from the original embedding space", nargs=1, type=str)

        # DATA MODELLING PARAMS

        parser.add_argument("-m", "--models", help="The data models to be created using the dataset. Choices: lr, nn, rnn, bi-lstm, cnn", nargs="*", type=str, choices=["lr","nn","rnn", "bi-lstm", "cnn"])

        parser.add_argument("-ma", "--model_actions", help="The actions to be performed on the models. Choices: train, test, evaluate", nargs="*", type=str, choices=["train", "test", "evaluate"])

        parser.add_argument("-v", "--verbose", help="If you want to see a few examples run through the pipeline from the test data.\n\tSpecify a number of examples", nargs=1, type=int, choices=range(1,4))

        # GLOBAL PARAMS IN CASE DATASET PARAMS ARE NOT INITIALIZED

        parser.add_argument("-dn", "--dataset_name", help="Name of the dataset to be used; in case datasets are not intialized", nargs=1, type=str)

        parser.add_argument("-sn", "--space_name", help="Name of the space to be used; in case datasets and space are not initialized", nargs=1, type=str)

        parser.add_argument("-drd", "--data_root_dir", help="Location of the main root directory, in case datasets have not been initialized", nargs=1, type=str)

        parser.add_argument("-ea", "--evaluation_actions", help="The evaluations to be performed on the models. Choices: accuracy-one-hot, precision, recall, fscore", nargs="*", type=str, choices=["accuracy-one-hot", "precision", "recall", "fscore"])

        parser.add_argument("-mp", "--multi_processing", help="Turn multiprocessing OFF or ON", nargs=1, choices=["True", "False"])

        parser.add_argument("-gpu", "--gpu", help="The switch to indicate if a GPU is supposed to be sued or not. Default: False", nargs=1, choices=["True", "False"])

        return parser.parse_args(commandline_args)

    def __initialize_data_params(self, are_validated, parsed_args):
        """
        Method which initializes the parameters required for data processing,
        dataset creation and vector space processing

        :are_validated (bool) : Flag which determines if the program will run further
        :parsed_args Namespace(option=list(str)|list(int)|str) : parsed commandline_args

        :return NULL
        """

        if parsed_args.dataset_type and parsed_args.dataset_dir is None:
                print("parased_dataset_type requires parsed_args.dataset_dir to be set")
                exit(1)

        if parsed_args.dataset_action and (parsed_args.dataset_type is None or parsed_args.dataset_dir is None):
                print("parsed_args.dataset_action requires parsed_args.dataset_type and parsed_args.dataset_dir set")
                exit(1)

        if parsed_args.dataset_dir:

            # Initialize dataset directory
            self.__data_params['dataset_dir'] = os.path.abspath(parsed_args.dataset_dir[0])
            if not os.path.isdir(self.__data_params['dataset_dir']):
                are_validated = False
                print("The dataset directory %s does not look like a valid directory" % self.__data_params['dataset_dir'])
                exit(1)

            # Set dataset name and dir
            # Currently a static assumption based on the bitbucket repo structure
            self.__data_params['dataset_name'] = os.path.basename(self.__data_params['dataset_dir'])
            self.__data_params['data_root_dir'] = os.path.dirname(os.path.dirname(self.__data_params['dataset_dir']))
            result_dir = os.path.join(self.__data_params['data_root_dir'], "results")
            result_statistics_dir = os.path.join(self.__data_params['data_root_dir'], "results", "statistics")
            vector_space = os.path.join(self.__data_params['data_root_dir'], "vector_space")
            vector_datasets = os.path.join(self.__data_params['data_root_dir'], "vector_datasets")
            if not os.path.isdir(result_dir):
                os.makedirs(result_dir)
            self.__data_params['result_dir'] = result_dir
            if not os.path.isdir(result_statistics_dir):
                os.makedirs(result_statistics_dir)
            self.__data_params['result_statistics_dir'] = result_statistics_dir
            if not os.path.isdir(vector_space):
                os.makedirs(vector_space)
            self.__data_params['vector_space'] = vector_space
            if not os.path.isdir(vector_datasets):
                os.makedirs(vector_datasets)
            self.__data_params['vector_datasets'] = vector_datasets

            self.__dataset_name = self.__data_params['dataset_name']
            self.__data_root_dir = self.__data_params['data_root_dir']


            # Initialize dataset files to be read
            if not parsed_args.dataset_type:
                are_validated = False
                print("The types of datasets to be read need to be specified.\nType python3 run_program.py --help")
                exit(1)
            else:
                self.__data_params['dataset_files'] = {}
                for each_dataset_type in parsed_args.dataset_type:
                    file = self.io.search_files(path=self.__data_params['dataset_dir'],
                                                search_string=each_dataset_type, match_type="partial")
                    if not len(file) == 1:
                        are_validated = False
                        if len(file) < 1:
                            print("%s : %s :: No file found!" % (self.__data_params['dataset_dir'],
                                                                 each_dataset_type))
                        elif len(file) > 1:
                            print("%s : %s :: More than 1 file(s) found" % (self.__data_params['dataset_dir'],
                                                                 each_dataset_type))
                            print("Files:\n\t", files)
                        exit(1)
                    self.__data_params['dataset_files'][each_dataset_type] = file[0]

            # Initialize dataset actions
            if parsed_args.dataset_action:
                self.__data_params['dataset_actions'] = parsed_args.dataset_action
                if ("build_dataset" or "extract_embeddings") in self.__data_params['dataset_actions']:
                    if "read" not in self.__data_params['dataset_actions']:
                        print("'build_dataset' / 'extract embeddings' in parsed_args.dataset_action requires 'read'.\n\tAdding 'read' to parsed_args.dataset_action")
                        self.__data_params['dataset_actions'].append("read")
            else:
                self.__data_params['dataset_actions'] = ["read"]

            # Initialize space file
            if parsed_args.space_file:
                self.__data_params['space_file'] = os.path.abspath(parsed_args.space_file[0])
                if not os.path.isfile(self.__data_params['space_file']):
                    are_validated = False
                    print("The Space file %s does not look like a valid file" % self.__data_params['space_file'])
                    exit(1)

            # Initialize dataset space file
            if parsed_args.dataset_space_file:
                self.__data_params['dataset_space_file'] = os.path.abspath(parsed_args.dataset_space_file[0])
                if not os.path.isfile(self.__data_params['dataset_space_file']):
                    are_validated = False
                    print("The Dataset space file %s does not look like a valid file" % self.__data_params['dataset_space_file'])
                    exit(1)
        return

    def __initialize_model_params(self, are_validated, parsed_args):
        """
        Method which initializes the parameters required for modelling

        :are_validated (bool) : Flag which determines if the program will run further
        :parsed_args Namespace(option=list(str)|list(int)|str) : parsed commandline_args

        :return NULL
        """

        if parsed_args.model_actions and not parsed_args.models:
            are_validated = False
            print("parsed_args.model_actions requires initializing parsed_args.models")

        if parsed_args.evaluation_actions and (not parsed_args.model_actions or not parsed_args.models):
            are_validated = False
            print("parsed_args.evaluation_actions requires BOTH parsed_args.model_actions and parsed_args.models")

        if parsed_args.models:
            # Initialize model types
            self.__model_params['models'] = [each_model.lower() for each_model in parsed_args.models]

            # Initialize actions to be performed on the models
            if parsed_args.model_actions:
                self.__model_params['model_actions'] = [each_action.lower() for each_action in parsed_args.model_actions]
            # else:
            #     self.__model_params['model_actions'] = ["train", "test", "evaluate"]

            # Initialize actions to be performed on the models
            if parsed_args.evaluation_actions:
                self.__model_params['evaluation_actions'] = [(each_exp, each_evaluation) for each_exp in self.__model_params['experiments'] for each_evaluation in parsed_args.evaluation_actions if (each_exp, each_evaluation) in self.__evaluations_allowed]
            else:
                self.__model_params['evaluation_actions'] = self.__evaluations_allowed
        return

    def __final_validation(self, are_validated, parsed_args):
        """
        Method which does a final evaluation on inter-related parameters

        :are_validated (bool) : Flag which determines if the program will run further
        :parsed_args Namespace(option=list(str)|list(int)|str) : parsed commandline_args

        :return NULL
        """
        are_validated = True

        # The program SHOULD have atleast 1 set of actions to perform; on the dataset OR on the models
        if not "dataset_actions" in self.__data_params and not "model_actions" in self.__model_params:
            are_validated = False
            print("No dataset OR modelling actions specified.")
        else:
            if "dataset_actions" in self.__data_params:
                if "extract_embeddings" in self.__data_params['dataset_actions']:
                    if not parsed_args.space_file:
                        are_validated = False
                        print("'extract_embeddings' in parsed_args.dataset_action requires a path for the original embeddings.\nType python3 run_program.py --help")

                if "build_dataset" in self.__data_params['dataset_actions']:
                    if "extract_embeddings" not in self.__data_params['dataset_actions']:
                        vector_space_dir = os.path.join(self.__data_params['data_root_dir'], "vector_space")
                        if os.path.isdir(vector_space_dir):
                            files = self.io.search_files(path=vector_space_dir, search_string="space", match_type="partial")
                            if not any([True if each_file.find(self.__dataset_space_name) > -1 else False for each_file in files]):
                                are_validated = False
                                print("'build_dataset' in parsed_args.dataset_action requires a dataset vector space which is either created (first time) by 'extract_embeddings' OR needs to be supplied as a command line argument.\nType python3 run_program.py --help")

                            else:
                                for each_file in files:
                                    if each_file.find(self.__dataset_space_name) > -1:
                                        self.__data_params['dataset_space_file'] = each_file
                                        break
                                if len(files) > 1:
                                    print("Warning: More than 1 files (above) for the given dataset have been identified.")
                                    print("Maybe there are vector spaces created from different corpora for the same dataset vocab")
                                    print("Choose 1 specific filename as input")
                                    print("CURRENTLY SELECTED DEFAULT DATASET SPACE FILE: << %s >>" % self.__data_params['dataset_space_file'])

                    if ("models" and "experiments") not in self.__model_params:
                        print("To build datasets, model_params need to be initialized with 'models' and 'experiments'")
                        are_validated = False

                    if "dataset_name" not in self.__data_params:
                        print("Cannot build datasets without and dataset details. Specify directory and dataset types to read")
                        are_validated = False

        return are_validated



if __name__=="__main__":
    if len(sys.argv[1:]) < 1:
        print("This program requires command line arguments.\n\tHelp: python3 run_program.py -h | --help")
        exit(1)

    ac = AtisnluController()
    is_executed = ac.run_program(commandline_args=sys.argv[1:])
    if is_executed:
        print("Program terminated successfully")
    else:
        print("Program failed to execute properly!!")
